import React, {Component} from 'react'
import consts from "../common/consts";
import axios from "axios";
import {sendError} from "../common/utils";
import TableArray from "../common/layout/TableArray";
import Grid from "../common/layout/Grid";
import Row from "../common/layout/Row";
import TableObject from "../common/layout/TableObject";
import MapWidget from "../common/widget/MapWidget";
import moment from "moment";
import loading_icon from "../assets/img/loading_icon.gif";
import OpenMapComponent from "./OpenMapComponent";
import {toastr} from "react-redux-toastr";


export default class OBDSensores extends Component {
  constructor(props) {
    super(props);

    this.state = {
      st500pid : {},
      pids:[],
      map: {},
      google:{},
      center:  [-3.802774, -38.536376],
    }
  }



  componentDidMount() {
    const url = `${consts.API_URL}/sensores/${this.props.placa}`

    axios.get(url)
      .then(resp => {
        this.setState(prevState => ({pids:resp.data.pids}))
        const deviceDate = moment(resp.data.deviceDate).format('DD/MM/YYYY HH:mm:ss')
        const storeDate = moment(resp.data.storeDate).format('DD/MM/YYYY HH:mm:ss')
        const clone = {...resp.data,deviceDate,storeDate}
        delete clone.pids
        this.setState(prevState => ({st500pid:clone}))
        this.setState(s=>({center:[clone.latitude,clone.longitude]}))
      })
      .catch(e => {
        toastr.error('Erro', "Veículo sem dados de sensores...")
        //sendError(e)
      })
  }

  onMapReady(mapProps, map) {
    this.setState(prevState => ({map: map, google: mapProps.google}))
  }

  render() {

    if(!this.state.pids  || this.state.pids.length==0 || !this.state.st500pid.latitude)
      return (<div className="text-center">
        Veículo sem dados de sensores...
        {/*<img src={loading_icon}/>*/}
      </div>)

    const clone = {...this.state.st500pid}

    //const center = [this.state.st500pid.latitude, this.state.st500pid.longitude]
    console.log("clone",clone)



    return (
      <Row>
        <Grid cols="12 4" className="maxHeight maxWidth padding-0">
          <TableObject label="Momento/Local da Captura" object={this.state.st500pid}></TableObject>
          <label htmlFor="mapa">Localização da Captura</label>
          <OpenMapComponent center={this.state.center} showViatura={true} message={this.state.st500pid.deviceDate} zoom={14} style={{width:  '100%',height: '200px'}}  />
        </Grid>

        <Grid cols="12 8">
          <TableArray label="Dados dos Sensores" list={this.state.pids}></TableArray>


        </Grid>
      </Row>

    )





  }


}


