import React, {createRef, Component} from 'react'
import TileLayer from "react-leaflet/es/TileLayer";
import Map from "react-leaflet/es/Map";
import Marker from "react-leaflet/es/Marker";
import Popup from "react-leaflet/es/Popup";
import Polyline from "react-leaflet/es/Polyline";
import If from "../common/operator/If";
import L from 'leaflet'


export const pointerIcon = new L.Icon({
  iconUrl: require('../assets/img/pickup.gif'),
  iconAnchor: [24, 16],
  popupAnchor: [24, 16],
  iconSize: [48, 31],
})

export default class OpenMapComponent extends Component {

  constructor(props) {
    super(props);
  }



  componentDidMount() {
  }


  state = {
    lat: -3.802774,
    lng: - 38.536376,
    zoom: this.props.zoom,

  }


  render() {

    /*onMapReady={(a,b)=>this.onMapReady(a,b)} polyline={this.state.polyline}*/



    return (



       <Map center={this.props.center}
             style={this.props.style}
             zoom={this.state.zoom}
             onClick={this.handleClick}
             onZoomend={(e)=>this.onZoomEvent(e)}
       >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        <If test={this.props.polyline} >
          <Polyline color="blue" positions={this.props.polyline} />
        </If>

         <If test={this.props.showViatura} >
        <Marker position={this.props.center} icon={pointerIcon}>
          <Popup>
            Data: {this.props.message}
          </Popup>
        </Marker>
         </If>

      </Map>

      );
  }

  handleZoomchange(e) {

    console.log("ZOONCHANGE",e)

  }

  onZoomEvent(e) {
    console.log("ZOONCHANGE",e.target._zoom)
    this.setState(prevState => ({zoom: e.target._zoom }))
  }
}










