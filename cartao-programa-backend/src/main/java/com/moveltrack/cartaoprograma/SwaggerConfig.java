package com.moveltrack.cartaoprograma;

import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
import static springfox.documentation.builders.PathSelectors.*;

@Import({springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration.class})
@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfig extends WebMvcConfigurationSupport {

  @Bean
  public Docket greetingApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
            //.apis(RequestHandlerSelectors.withClassAnnotation(RepositoryRestResource.class))
            //.apis(RequestHandlerSelectors.withClassAnnotation(BasePathAwareController.class))
            .apis(RequestHandlerSelectors.any())
            //.paths(PathSelectors.any())
           // .apis(not(withClassAnnotation(CustomIgnore.class))
                 //   .apis(not(RequestHandlerSelectors.withClassAnnotation(Api.class))/*basePackage("com.moveltrack.cartaoprograma.rest.api.controller")*/)
        .build()
        .apiInfo(metaData());

  }

  private ApiInfo metaData() {
    return new ApiInfoBuilder()
        .title("Cartão Programa REST API")
        .description("API de Suporte para o projeto Cartão Programa (Rastreamento padrão OBD-II) ")
        .version("1.0.0")
        .license("Jessé Lima Sá")
        .licenseUrl("mailto:jlimasa@gmail.com")
        .build();
  }

  @Override
  protected void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
        .addResourceLocations("classpath:/META-INF/resources/");

    registry.addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }
}
