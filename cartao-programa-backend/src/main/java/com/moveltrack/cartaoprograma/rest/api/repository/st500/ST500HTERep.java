package com.moveltrack.cartaoprograma.rest.api.repository.st500;

import com.moveltrack.cartaoprograma.model.st500.ST500EMG;
import com.moveltrack.cartaoprograma.model.st500.ST500HTE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ST500HTERep extends JpaRepository<ST500HTE,Long>{

	List<ST500HTE> findTop10BySerialOrderByDeviceDateDesc(String serial);


}
