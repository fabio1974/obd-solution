package com.moveltrack.cartaoprograma.rest.api.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.moveltrack.cartaoprograma.model.st500.*;
import com.moveltrack.cartaoprograma.rest.api.repository.st500.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moveltrack.cartaoprograma.model.Viatura;
import com.moveltrack.cartaoprograma.rest.api.repository.GeoEnderecoRep;
import com.moveltrack.cartaoprograma.rest.api.repository.ViaturaRepository;


@BasePathAwareController
public class OBDController {
	
	@Autowired ST500PIDRep pdiRep;
	@Autowired ST500ALTRep altRep;
	@Autowired ST500EMGRep emgRep;
	@Autowired ST500HTERep hteRep;
	@Autowired ST500OTERep oteRep;

	@Autowired ViaturaRepository viaturaRepository;

	@ApiOperation(value = "Pega a última leitura dos Sensores do OBD para determinado veículo")
    @ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "sensores/{placa}")
	public ST500PID sensores(@PathVariable("placa")String placa) throws Exception {
		
		Viatura viatura = viaturaRepository.findByPlaca(placa);
		String imei = viatura.getRastreador().getImei();
		String serial = String.valueOf(Integer.parseInt(imei));
		ST500PID st500pid = pdiRep.findTopBySerialOrderByDeviceDateDesc(serial);
		
		List<PID> pids = new ArrayList<PID>();
		
		if(st500pid!=null) {
			pids = st500pid.getPids();
			for (PID pid : pids) {
				PID aux = PID.getPid(pid.getHex());
				pid.setDescryption(aux.getDescryption());
				pid.setUnit(aux.getUnit());
				pid.setHexStr(String.format("%02x",pid.getHex()).toUpperCase());
			}
		}

		if(st500pid==null)
			throw new Exception("Sem sensor's!");
		
		return st500pid;
	}
	
	


	@Autowired GeoEnderecoRep geoEnderecoRep;

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	@ApiOperation(value = "Pega a última leitura dos Sinais de Alertas do OBD para determinado veículo")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET,value="/alertas/{placa}")
	public List<ST500ALT> alerta(@PathVariable("placa") String placa) {
		try{
			Viatura viatura = viaturaRepository.findByPlaca(placa);
			String imei = viatura.getRastreador().getImei();
			String serial = String.valueOf(Integer.parseInt(imei));
			List<ST500ALT> list = altRep.findTop10BySerialOrderByDeviceDateDesc(serial);
			for (ST500ALT st500alt : list) {
				st500alt.setAddress(geoEnderecoRep.getAddressFromLocation(st500alt.getLatitude(),st500alt.getLongitude(),true));
				st500alt.setAlerta();
				st500alt.setData(sdf.format(st500alt.getDeviceDate()));

			}
			Collections.reverse(list);
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@ApiOperation(value = "Pega os 10 últimas leitura dos Sinais de Emergência do OBD para determinado veículo")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET,value="/emergencias/{placa}")
	public List<ST500EMG> emergencia(@PathVariable("placa") String placa) {

		try{
			Viatura viatura = viaturaRepository.findByPlaca(placa);
			String imei = viatura.getRastreador().getImei();
			String serial = String.valueOf(Integer.parseInt(imei));
			List<ST500EMG> list = emgRep.findTop10BySerialOrderByDeviceDateDesc(serial);
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}

	}


	@ApiOperation(value = "Pega os 10 últimos relátorio de Histograma de Velocidade (km/h) pela placa do veículo")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET,value="/histogramaVelocidade/{placa}")
	public List<ST500HTE> histogramaVelocidade(@PathVariable("placa") String placa) {
		try{
			Viatura viatura = viaturaRepository.findByPlaca(placa);
			String imei = viatura.getRastreador().getImei();
			String serial = String.valueOf(Integer.parseInt(imei));
			List<ST500HTE> list = hteRep.findTop10BySerialOrderByDeviceDateDesc(serial);
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}

	}

	@ApiOperation(value = "Pega os 10 últimos relátorio de Histograma de Rotação (RPM) pela placa do veículo")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET,value="/histogramaRPM/{placa}")
	public List<ST500OTE> histogramaRPM(@PathVariable("placa") String placa) {
		try{
			Viatura viatura = viaturaRepository.findByPlaca(placa);
			String imei = viatura.getRastreador().getImei();
			String serial = String.valueOf(Integer.parseInt(imei));
			List<ST500OTE> list = oteRep.findTop10BySerialOrderByDeviceDateDesc(serial);
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


}
