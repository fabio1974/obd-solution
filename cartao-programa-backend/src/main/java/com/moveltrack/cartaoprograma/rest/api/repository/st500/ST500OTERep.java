package com.moveltrack.cartaoprograma.rest.api.repository.st500;

import com.moveltrack.cartaoprograma.model.st500.ST500EMG;
import com.moveltrack.cartaoprograma.model.st500.ST500OTE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ST500OTERep extends JpaRepository<ST500OTE,Long>{

	List<ST500OTE> findTop10BySerialOrderByDeviceDateDesc(String serial);

}
