package com.moveltrack.cartaoprograma.rest.api.controller;

import com.moveltrack.cartaoprograma.model.Viatura;
import com.moveltrack.cartaoprograma.rest.api.repository.ViaturaRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moveltrack.cartaoprograma.model.Perfil;
import com.moveltrack.cartaoprograma.rest.api.repository.PerfilRepository;
import com.moveltrack.cartaoprograma.rest.get.specification.SpecificationsBuilder;


@BasePathAwareController
public class ViaturaController  {

	@Autowired
    ViaturaRepository repository;

    @ResponseBody
    @ApiOperation(value = "pesquisa por veículos")
    @RequestMapping(method = RequestMethod.GET, value = "viaturas")
    public Page<Viatura> search(@RequestParam(value = "search",required=false) String search, Pageable pageable) {

    	SpecificationsBuilder<Viatura> ms = new SpecificationsBuilder<Viatura>();
        return repository.findAll(ms.buildEspecification(search),pageable);
        
    }
}
