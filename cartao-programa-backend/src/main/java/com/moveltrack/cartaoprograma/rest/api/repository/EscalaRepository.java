package com.moveltrack.cartaoprograma.rest.api.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.moveltrack.cartaoprograma.model.Escala;

@RepositoryRestResource(path = "escalas")
public interface EscalaRepository extends PagingAndSortingRepository<Escala,Integer>,JpaSpecificationExecutor<Escala> {


}