package com.moveltrack.cartaoprograma.rest.api.controller;

import com.moveltrack.cartaoprograma.model.Perfil;
import com.moveltrack.cartaoprograma.rest.api.repository.PerfilRepository;
import com.moveltrack.cartaoprograma.rest.get.specification.SpecificationsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@BasePathAwareController
public class PerfilController  {

	@Autowired
    PerfilRepository repository;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "perfis")
    public Page<Perfil> search(@RequestParam(value = "search",required=false) String search, Pageable pageable) {

    	SpecificationsBuilder<Perfil> ms = new SpecificationsBuilder<Perfil>();
        return repository.findAll(ms.buildEspecification(search),pageable);
        
    }
}
