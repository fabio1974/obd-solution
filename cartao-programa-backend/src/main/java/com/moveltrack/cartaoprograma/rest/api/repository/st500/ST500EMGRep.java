package com.moveltrack.cartaoprograma.rest.api.repository.st500;

import com.moveltrack.cartaoprograma.model.st500.ST500ALT;
import com.moveltrack.cartaoprograma.model.st500.ST500EMG;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ST500EMGRep extends JpaRepository<ST500EMG,Long>{

	List<ST500EMG> findTop10BySerialOrderByDeviceDateDesc(String serial);


}
