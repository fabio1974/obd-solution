package com.moveltrack.cartaoprograma.model;


public enum DrogaApreendidaTipo {
    ANFETAMINA,
    BARBITIRICOS, 
    CHA_CORGUMELO, 
    CLORIFORMIO, 
    COCAINA, 
    CRACK, 
    ECSTASY, 
    HEROINA,
    INALANTES,
    LSD,
    MACONHA,
    MORFINA,
    SKANK, 
    OPIO, 
    OUTRAS
}


