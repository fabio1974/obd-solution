package com.moveltrack.cartaoprograma.model.st500;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Entity
public class ST500OTE {

	
	
	static String ST500OTEStr = "ST500OTE;205000050;07;675;20151127;05:32:59;13.00;0.0;1;61;157;2963;8626;15;13;16;6;1;1;1;6;4;0;0;28;103;0;1075";

	
	public static void main(String[] args) {

		ST500OTE teste = parse(ST500OTEStr.split(";"));
		teste.getTravelDistance();

	}


	public static ST500OTE parse(String[] fields) {
		ST500OTE res = new ST500OTE();

		try {res.setSerial(fields[1]);}catch(Exception e){}
		try {res.setModel(fields[2]);}catch(Exception e){}
		try {res.setSwVersion(fields[3]);}catch(Exception e){}

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
			Date d1= format.parse(fields[4]+" "+fields[5]);
			Calendar c = Calendar.getInstance();
			c.setTime(d1);
			c.add(Calendar.MINUTE,-180);
			res.setDeviceDate(c.getTime());
		}catch(Exception e){}

		try {res.setVolt(Double.parseDouble(fields[6]));}catch(Exception e){}
		try {res.setBackupBatteryVoltage(Double.parseDouble(fields[7]));}catch(Exception e){}
		try {res.setOnline(Integer.parseInt(fields[8]));}catch(Exception e){}
		
		try {res.setAverageSpeed(Double.parseDouble(fields[9]));}catch(Exception e){}
		try {res.setMaxSpeed(Double.parseDouble(fields[10]));}catch(Exception e){}
		
		try {res.setAverageRPM(Double.parseDouble(fields[11]));}catch(Exception e){}
		try {res.setMaxRPM(Double.parseDouble(fields[12]));}catch(Exception e){}
		
				
		try {res.setHistogramRPM00000_00999(Integer.parseInt(fields[13]));}catch(Exception e){}
		try {res.setHistogramRPM01000_01999(Integer.parseInt(fields[14]));}catch(Exception e){}
		try {res.setHistogramRPM02000_02999(Integer.parseInt(fields[15]));}catch(Exception e){}
		try {res.setHistogramRPM03000_03999(Integer.parseInt(fields[16]));}catch(Exception e){}
		try {res.setHistogramRPM04000_04999(Integer.parseInt(fields[17]));}catch(Exception e){}
		try {res.setHistogramRPM05000_05999(Integer.parseInt(fields[18]));}catch(Exception e){}
		try {res.setHistogramRPM06000_06999(Integer.parseInt(fields[19]));}catch(Exception e){}
		try {res.setHistogramRPM07000_07999(Integer.parseInt(fields[20]));}catch(Exception e){}
		try {res.setHistogramRPM08000_08999(Integer.parseInt(fields[21]));}catch(Exception e){}
		try {res.setHistogramRPM09000_09999(Integer.parseInt(fields[22]));}catch(Exception e){}
		try {res.setHistogramRPM10000_10999(Integer.parseInt(fields[23]));}catch(Exception e){}
		
		try {res.setAverageEngineCoolantTemperature(Integer.parseInt(fields[24]));}catch(Exception e){}
		try {res.setMaxEngineCoolantTemperature(Integer.parseInt(fields[25]));}catch(Exception e){}
		
		try {res.setDtcCnt(fields[26]);}catch(Exception e){}
		try {res.setTravelDistance(Integer.parseInt(fields[27]));}catch(Exception e){}
		


		return res;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition="serial")
	private Long id;

	private String serial;
	private String model;
	private String swVersion;
	
	private Date deviceDate;

	private Double volt;
	private Double backupBatteryVoltage;
	private Integer online;

	private Double averageSpeed;
	private Double maxSpeed;
	
	private Double averageRPM;
	private Double maxRPM;


	private Integer histogramRPM00000_00999;
	private Integer histogramRPM01000_01999;
	private Integer histogramRPM02000_02999;
	private Integer histogramRPM03000_03999;
	private Integer histogramRPM04000_04999;
	private Integer histogramRPM05000_05999;
	private Integer histogramRPM06000_06999;
	private Integer histogramRPM07000_07999;
	private Integer histogramRPM08000_08999;
	private Integer histogramRPM09000_09999;
	private Integer histogramRPM10000_10999;

	private Integer maxEngineCoolantTemperature;
	private Integer averageEngineCoolantTemperature;
	
	private String dtcCnt;
	
	private Integer travelDistance;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getSerial() {
		return serial;
	}


	public void setSerial(String serial) {
		this.serial = serial;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getSwVersion() {
		return swVersion;
	}


	public void setSwVersion(String swVersion) {
		this.swVersion = swVersion;
	}


	public Date getDeviceDate() {
		return deviceDate;
	}


	public void setDeviceDate(Date deviceDate) {
		this.deviceDate = deviceDate;
	}


	public Double getVolt() {
		return volt;
	}


	public void setVolt(Double volt) {
		this.volt = volt;
	}


	public Double getBackupBatteryVoltage() {
		return backupBatteryVoltage;
	}


	public void setBackupBatteryVoltage(Double backupBatteryVoltage) {
		this.backupBatteryVoltage = backupBatteryVoltage;
	}


	public Integer getOnline() {
		return online;
	}


	public void setOnline(Integer online) {
		this.online = online;
	}


	public Double getAverageSpeed() {
		return averageSpeed;
	}


	public void setAverageSpeed(Double averageSpeed) {
		this.averageSpeed = averageSpeed;
	}


	public Double getMaxSpeed() {
		return maxSpeed;
	}


	public void setMaxSpeed(Double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}


	public Double getAverageRPM() {
		return averageRPM;
	}


	public void setAverageRPM(Double averageRPM) {
		this.averageRPM = averageRPM;
	}


	public Double getMaxRPM() {
		return maxRPM;
	}


	public void setMaxRPM(Double maxRPM) {
		this.maxRPM = maxRPM;
	}


	public Integer getHistogramRPM00000_00999() {
		return histogramRPM00000_00999;
	}


	public void setHistogramRPM00000_00999(Integer histogramRPM00000_00999) {
		this.histogramRPM00000_00999 = histogramRPM00000_00999;
	}


	public Integer getHistogramRPM01000_01999() {
		return histogramRPM01000_01999;
	}


	public void setHistogramRPM01000_01999(Integer histogramRPM01000_01999) {
		this.histogramRPM01000_01999 = histogramRPM01000_01999;
	}


	public Integer getHistogramRPM02000_02999() {
		return histogramRPM02000_02999;
	}


	public void setHistogramRPM02000_02999(Integer histogramRPM02000_02999) {
		this.histogramRPM02000_02999 = histogramRPM02000_02999;
	}


	public Integer getHistogramRPM03000_03999() {
		return histogramRPM03000_03999;
	}


	public void setHistogramRPM03000_03999(Integer histogramRPM03000_03999) {
		this.histogramRPM03000_03999 = histogramRPM03000_03999;
	}


	public Integer getHistogramRPM04000_04999() {
		return histogramRPM04000_04999;
	}


	public void setHistogramRPM04000_04999(Integer histogramRPM04000_04999) {
		this.histogramRPM04000_04999 = histogramRPM04000_04999;
	}


	public Integer getHistogramRPM05000_05999() {
		return histogramRPM05000_05999;
	}


	public void setHistogramRPM05000_05999(Integer histogramRPM05000_05999) {
		this.histogramRPM05000_05999 = histogramRPM05000_05999;
	}


	public Integer getHistogramRPM06000_06999() {
		return histogramRPM06000_06999;
	}


	public void setHistogramRPM06000_06999(Integer histogramRPM06000_06999) {
		this.histogramRPM06000_06999 = histogramRPM06000_06999;
	}


	public Integer getHistogramRPM07000_07999() {
		return histogramRPM07000_07999;
	}


	public void setHistogramRPM07000_07999(Integer histogramRPM07000_07999) {
		this.histogramRPM07000_07999 = histogramRPM07000_07999;
	}


	public Integer getHistogramRPM08000_08999() {
		return histogramRPM08000_08999;
	}


	public void setHistogramRPM08000_08999(Integer histogramRPM08000_08999) {
		this.histogramRPM08000_08999 = histogramRPM08000_08999;
	}


	public Integer getHistogramRPM09000_09999() {
		return histogramRPM09000_09999;
	}


	public void setHistogramRPM09000_09999(Integer histogramRPM09000_09999) {
		this.histogramRPM09000_09999 = histogramRPM09000_09999;
	}


	public Integer getHistogramRPM10000_10999() {
		return histogramRPM10000_10999;
	}


	public void setHistogramRPM10000_10999(Integer histogramRPM10000_10999) {
		this.histogramRPM10000_10999 = histogramRPM10000_10999;
	}


	public Integer getMaxEngineCoolantTemperature() {
		return maxEngineCoolantTemperature;
	}


	public void setMaxEngineCoolantTemperature(Integer maxEngineCoolantTemperature) {
		this.maxEngineCoolantTemperature = maxEngineCoolantTemperature;
	}


	public Integer getAverageEngineCoolantTemperature() {
		return averageEngineCoolantTemperature;
	}


	public void setAverageEngineCoolantTemperature(Integer averageEngineCoolantTemperature) {
		this.averageEngineCoolantTemperature = averageEngineCoolantTemperature;
	}


	public String getDtcCnt() {
		return dtcCnt;
	}


	public void setDtcCnt(String dtcCnt) {
		this.dtcCnt = dtcCnt;
	}


	public Integer getTravelDistance() {
		return travelDistance;
	}


	public void setTravelDistance(Integer travelDistance) {
		this.travelDistance = travelDistance;
	}

	
		

	

}
