package com.moveltrack.cartaoprograma.model;
// Generated 23/07/2018 04:54:41 by Hibernate Tools 4.3.5.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Entity
@Table(name = "afinidade", schema = "public")
public class Afinidade implements java.io.Serializable {

	private AfinidadeId id;
	private Pessoa pessoaByPessoaId;
	private Pessoa pessoaByAfinidadeId;

	public Afinidade() {
	}

	public Afinidade(AfinidadeId id) {
		this.id = id;
	}

	public Afinidade(AfinidadeId id, Pessoa pessoaByPessoaId, Pessoa pessoaByAfinidadeId) {
		this.id = id;
		this.pessoaByPessoaId = pessoaByPessoaId;
		this.pessoaByAfinidadeId = pessoaByAfinidadeId;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "afinidadeId", column = @Column(name = "afinidade_id")),
			@AttributeOverride(name = "pessoaId", column = @Column(name = "pessoa_id")) })
	public AfinidadeId getId() {
		return this.id;
	}

	public void setId(AfinidadeId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
	public Pessoa getPessoaByPessoaId() {
		return this.pessoaByPessoaId;
	}

	public void setPessoaByPessoaId(Pessoa pessoaByPessoaId) {
		this.pessoaByPessoaId = pessoaByPessoaId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "afinidade_id", insertable = false, updatable = false)
	public Pessoa getPessoaByAfinidadeId() {
		return this.pessoaByAfinidadeId;
	}

	public void setPessoaByAfinidadeId(Pessoa pessoaByAfinidadeId) {
		this.pessoaByAfinidadeId = pessoaByAfinidadeId;
	}

}
