package com.moveltrack.cartaoprograma.model;
// Generated 23/07/2018 04:54:41 by Hibernate Tools 4.3.5.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * AreaOrganizacional generated by hbm2java
 */
@Entity
public class AreaOrganizacional implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition="serial")
	private int id;
	
	

	
	private String descricao;
	private String sigla;
	
    @Enumerated(EnumType.STRING)
	private AreaOrganizacionalTipo tipo;

    @ManyToOne
    //@JoinColumn(columnDefinition="integer", name="escala_tipo_id")
    private EscalaTipo escalaTipo;
    
    private Integer rendicaoHora;
	private Integer rendicaoMinuto;
    

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public AreaOrganizacionalTipo getTipo() {
		return tipo;
	}
	public void setTipo(AreaOrganizacionalTipo tipo) {
		this.tipo = tipo;
	}
	public EscalaTipo getEscalaTipo() {
		return escalaTipo;
	}
	public void setEscalaTipo(EscalaTipo escalaTipo) {
		this.escalaTipo = escalaTipo;
	}
	public Integer getRendicaoHora() {
		return rendicaoHora;
	}
	public void setRendicaoHora(Integer rendicaoHora) {
		this.rendicaoHora = rendicaoHora;
	}
	public Integer getRendicaoMinuto() {
		return rendicaoMinuto;
	}
	public void setRendicaoMinuto(Integer rendicaoMinuto) {
		this.rendicaoMinuto = rendicaoMinuto;
	}

	
	
	

}
