package com.moveltrack.cartaoprograma.model;


public enum AfastamentoTipo {
    FERIAS, DOENCA, LICENSA
}
