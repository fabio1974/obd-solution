package com.moveltrack.cartaoprograma.model;


public enum OperacaoStatus {
    AGENDADA, INICIADA, FINALIZADA, CANCELADA
}
