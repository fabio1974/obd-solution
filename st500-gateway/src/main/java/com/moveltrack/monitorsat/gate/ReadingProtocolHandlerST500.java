package com.moveltrack.monitorsat.gate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.moveltrack.monitorsat.gate.model.Location;
import com.moveltrack.monitorsat.gate.model.Location2;
import com.moveltrack.monitorsat.gate.model.st500.PID;
import com.moveltrack.monitorsat.gate.model.st500.ST500ALT;
import com.moveltrack.monitorsat.gate.model.st500.ST500EMG;
import com.moveltrack.monitorsat.gate.model.st500.ST500HTE;
import com.moveltrack.monitorsat.gate.model.st500.ST500OTE;
import com.moveltrack.monitorsat.gate.model.st500.ST500PID;
import com.moveltrack.monitorsat.gate.model.st500.ST500STT;
import com.moveltrack.monitorsat.gate.repository.st500.PIDRep;
import com.moveltrack.monitorsat.gate.repository.st500.ST500ALTRep;
import com.moveltrack.monitorsat.gate.repository.st500.ST500EMGRep;
import com.moveltrack.monitorsat.gate.repository.st500.ST500HTERep;
import com.moveltrack.monitorsat.gate.repository.st500.ST500OTERep;
import com.moveltrack.monitorsat.gate.repository.st500.ST500PIDRep;
import com.moveltrack.monitorsat.gate.repository.st500.ST500STTRep;
import com.moveltrack.monitorsat.gate.util.Constantes;
import com.moveltrack.monitorsat.gate.util.Util;
import com.moveltrack.monitorsat.gate.util.Utils;

/**
 * 
 * @author mailto:jlimasa@gmail.com
 *
 */

@Component
@Scope("prototype")
public class ReadingProtocolHandlerST500 extends ReadingProtocolHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	boolean DEBUG_MODE = Constantes.DEBUG_ST500;
	
	public ReadingProtocolHandlerST500() {
	}
	
	@Override
	public void startReading(Socket socket) throws IOException {
		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int current = 0;
		byte[] array;

		Utils.log(DEBUG_MODE," - START READING PROTOCOL ST500");
		InputStream in = socket.getInputStream();
		while ((current=in.read()) != -1){
			
			buffer.write(current);
			if (current==0x0D && buffer.size()>3){
				array = buffer.toByteArray();
					String report = new String(array);
					trataReport(report);
					buffer = new ByteArrayOutputStream();
			}
		} 	
		Utils.log(DEBUG_MODE,"FIM DE LEITURA DO STREAM");
	}
	
	@Autowired ST500STTRep sttRep;
	@Autowired ST500ALTRep altRep;
	@Autowired ST500PIDRep pidRep;
	
	@Autowired ST500EMGRep emgRep;
	@Autowired ST500HTERep hteRep;
	@Autowired ST500OTERep oteRep;

	
	@Autowired PIDRep miniPidRep;
	
	public void trataReport(String report) {
		
		System.out.println(report);
		
		String[] fields =  report.split(";");
		
		if(fields[0].equals("ST500STT")) {//Status - String that device reports periodically. - dados de rastreamento, basicamente
			 ST500STT st500stt = ST500STT.parse(fields);
			 sttRep.save(st500stt);
			 
			 Location loc = new Location();
			 loc.setBattery(st500stt.getBatteryVoltage()>0?"1":"0");
			 loc.setComando("ST500STT");
			 loc.setDateLocation(st500stt.getDeviceDate());
			 loc.setDateLocationInicio(st500stt.getDeviceDate());
			 loc.setGps(st500stt.getGps()>0?"On":null);
			 //loc.setGsm(st500stt.get);
			 loc.setIgnition(st500stt.getIgnition()==null?null:st500stt.getIgnition()==0?"0":"1");
			 loc.setImei(String.format("%015d",Integer.parseInt(st500stt.getSerial())));
			 loc.setLatitude(st500stt.getLatitude());
			 loc.setLongitude(st500stt.getLongitude());
			 loc.setMcc(st500stt.getMcc()!=null?st500stt.getMcc():0);
			 loc.setSatelites(st500stt.getSatellite()!=null?st500stt.getSatellite():0);
			 loc.setVelocidade(st500stt.getSpeed()!=null?st500stt.getSpeed():0);
			 
			 if(loc.getVelocidade() < 2)
				 loc.setVelocidade(0);
			 
			 //loc.setVolt(st500stt.getBatteryVoltage()"");
			 locationRep.save(loc);
			 Location2 loc2 = Util.locationToLocation2(loc);
			 location2Rep.save(loc2);
			 
		}else if(fields[0].equals("ST500PID")) { //OBD string that device reports periodically. 
			 ST500PID st500pid = ST500PID.parse(fields);
			 saveST500PID(st500pid);
		}else if(fields[0].equals("ST500ALT")) { //Alertas tratados pela placa do ST500 - String that is sent when some special condition is occurred.
			 ST500ALT st500alt = ST500ALT.parse(fields);
			 altRep.save(st500alt);
		}else if(fields[0].equals("ST500EMG")) {// Sinais de Emergencia - String that is sent when emergency occurs. Emergency type	‘2’ = emergency by parking lock, ‘3’ = emergency by removing main power.
			 ST500EMG st500emg = ST500EMG.parse(fields);
			 emgRep.save(st500emg);
		}else if(fields[0].equals("ST500HTE")) {//Travel Event - String to report information of current travel when ignition is OFF (starts parking). SUmario de faixas de velocidade
			 ST500HTE st500hte = ST500HTE.parse(fields);
			 hteRep.save(st500hte);
		}else if(fields[0].equals("ST500OTE")) {//OBD Travel Event report - String to report information of current OBD travel when ignition is OFF (starts parking). SUmario de faixas de RPM
			 ST500OTE st500ote = ST500OTE.parse(fields);
			 oteRep.save(st500ote);
		}
		
	}

	@Transactional
	private void saveST500PID(ST500PID st500pid) {
		for (PID pid : st500pid.getPids()) {
			miniPidRep.save(pid);
		}
		pidRep.save(st500pid);
	}
	
	
}
