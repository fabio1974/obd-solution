package com.moveltrack.monitorsat.gate;

import java.net.Socket;

import com.moveltrack.monitorsat.gate.util.Utils;


/**
 * 
 * @author jlimasa@gmail.com
 *
 */

public class ReadingProtocolListenerConnections  extends Thread{
	Socket socket;
	
	ReadingProtocolHandler ph;
	
	public ReadingProtocolListenerConnections(ReadingProtocolHandler ph,Socket socket){
		this.socket = socket;
		this.ph = ph;
	}
	
	public void run() { 
		try{ 
			socket.setSoTimeout(10*60*1000);
			ph.startReading(socket);
		} 
		catch (Exception e){ 
			e.printStackTrace();
		}
		finally{ 
			try{
				if(socket!=null) 
					socket.close();
			}catch(Exception e){
					e.printStackTrace();
			} 
		}
	}
}


