package com.moveltrack.monitorsat.gate;

import java.io.IOException;
import java.net.ServerSocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.moveltrack.monitorsat.gate.repository.LocationRep;

/**
 * 
 * @author jlimasa@gmail.com
 *
 */

@Component
@Scope("prototype")
class ReadingProtocolListener extends Thread{

	private ReadingProtocolHandler ph;
	private int port;
	
	@Autowired LocationRep locationRep;


    @Override
	public void run() {
    	ServerSocket serverSocket = null;
		try{
			serverSocket = new ServerSocket(port);
			while(true){
				new ReadingProtocolListenerConnections(ph,serverSocket.accept()).start();  //Uma thread iniciada para cada rastreador
			}	
		}catch (Exception e) {
            try {
				serverSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
    
	public ReadingProtocolHandler getPh() {
		return ph;
	}
	public void setPh(ReadingProtocolHandler ph) {
		this.ph = ph;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}	