package com.moveltrack.monitorsat.gate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.moveltrack.monitorsat.gate.model.Location;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Repository
public interface LocationRep extends JpaRepository<Location,Long>, JpaSpecificationExecutor<Location>, LocationRepCustom {

	


}
