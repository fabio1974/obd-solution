package com.moveltrack.monitorsat.gate.util;

import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.moveltrack.monitorsat.gate.model.Location;
import com.moveltrack.monitorsat.gate.model.Rastreador;
import com.moveltrack.monitorsat.gate.repository.RastreadorRepository;
import com.moveltrack.monitorsat.gate.repository.ViaturaRepository;

/**
 * 
 * @author jlimasa@gmail.com
 *
 */

@Component
public class AlarmesEnviaEmail
{

	static String SOS = "SOS!";
	static String BATERIA_FRACA = "BATERIA FRACA!"; 
	static String DESCONECTANDO_BATERIA = "DESCONECTANDO BATERIA!";
	static String PARADA_BRUSCA = "PARADA BRUSCA!";
	static String ALARME_NEUTRO = "ALARME NEUTRO!";
	static String ENTROU_CERCA = "ENTROU NA CERCA VIRTUAL!";
	static String SAIU_CERCA = "SAIU DA CERCA VIRTUAL!";
	
	@Autowired RastreadorRepository equipamentoRep;
	@Autowired ViaturaRepository veiculoRep;

	public void gerenciaAlarme(Location alarme){
		Rastreador equipamento =  equipamentoRep.findByImei(alarme.getImei());
	} 	  

	public static void sendEmail(Location alarme,Rastreador equipamento/*,Cliente cliente,Veiculo veiculo,*/,String to){

		if(to==null)
			to = "fabio.barros1974@gmail.com";	   

		String from = "alarme@moveltrack.com.br";
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "mail.moveltrack.com.br");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		//props.put("mail.debug", "true");
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("alarme@moveltrack.com.br","mvt17547");
			}
		});
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));

			message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
			message.setSubject(getAlarmeByCodigo(alarme));

			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
			//messageBodyPart.setContent(getConteudo(alarme,cliente,equipamento,veiculo), "text/html; charset=ISO-8859-1");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart );
			Transport.send(message);
			//Utils.log(true,"Email de ALARME enviado para "+cliente.getNome()+ " e-mail:"+cliente.getEmail());
		}catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}




	private static String getConteudo(Location alarme/*, Pessoa cliente, Equipamento equipamento, Veiculo veiculo*/) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		StringBuffer mensagem = new StringBuffer("<p>Prezado cliente, um alarme foi enviado nesse momento do veículo ");
		//mensagem.append(veiculo.getMarcaModelo() +":</p>");
		mensagem.append("<br>");
		mensagem.append("<p>Tipo do Alarme: "+getAlarmeByCodigo(alarme).toUpperCase()+"</p>");
		mensagem.append("<p>Data: "+sdf.format(alarme.getDateLocation())+"h</p>");
		mensagem.append("<p>Latitude: "+alarme.getLatitude()+"</p>");
		mensagem.append("<p>Longitude: "+alarme.getLongitude()+"</p>");       	
		mensagem.append("<p>Velocidade: "+alarme.getVelocidade()+"</p>");       	
		//mensagem.append("<p>Cliente: "+cliente.getNome()+"</p>");
		mensagem.append("<p>Este e-mail � autom�tico e n�o recebe respostas.</b>.</p>");
		mensagem.append("<p>Por favor, em caso de d�vida, ligue para <b>4102-3953</b>.</p>");
		mensagem.append("<p><b>Moveltrack Rastreamento.</b></p>");	
		return mensagem.toString();
	}



	private static String getAlarmeByCodigo(Location loc) {
		if(loc!=null && loc.getAlarmType()!=null){
			if(loc.getAlarmType().equals("100")){
				return SOS;
			}	
			else if(loc.getAlarmType().equals("011")){
				return BATERIA_FRACA;
			}	
			else if(loc.getAlarmType().equals("010")){
				return DESCONECTANDO_BATERIA;
			}	
			else if(loc.getAlarmType().equals("001")){
				return  PARADA_BRUSCA;
			}	
			else if(loc.getAlarmType().equals("000")){
				return  ALARME_NEUTRO;
			}
			else if(loc.getAlarmType().equals("110")){
				return  ENTROU_CERCA;
			}	
			else if(loc.getAlarmType().equals("111")){
				return  SAIU_CERCA;
			}	

		}
		return null;
	}









	/*   private static void attach(Multipart multipart, byte[] pdfFile, String fileName) {
	   BodyPart messageBodyPart = new MimeBodyPart();
       DataSource source = new ByteArrayDataSource(pdfFile, "application/pdf");
       try {
		messageBodyPart.setDataHandler(new DataHandler(source));
	       messageBodyPart.setFileName(fileName);
	       multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
   }*/
}