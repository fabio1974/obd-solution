package com.moveltrack.monitorsat.gate.model;
public enum Operadora {
    CLARO("Claro"), 
    OI("Oi"), 
    TIM("Tim"), 
    VIVO("Vivo"); 
	
	private String descricao;

	private Operadora(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}