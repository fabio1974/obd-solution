package com.moveltrack.monitorsat.gate.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.moveltrack.monitorsat.gate.model.Rastreador;


/**
 * 
 * @author jlimasa@gmail.com
 *
 */


@RepositoryRestResource(path = "rastreadors")
public interface RastreadorRepository extends PagingAndSortingRepository<Rastreador,Integer> , JpaSpecificationExecutor<Rastreador>  {

	Rastreador findByImei(String imei);


}