package com.moveltrack.monitorsat.gate.component;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Component
@Scope("prototype")
public class PrintThread extends Thread{

	@Override
	public void run() {

		//System.out.println(getName() + " is running");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//System.out.println(getName() + " is running");
	}

}