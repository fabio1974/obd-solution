package com.moveltrack.monitorsat.gate.threadlocal;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
import com.moveltrack.monitorsat.gate.model.Location;

public class StatusLoc {

	public static final ThreadLocal<Location> instance = new ThreadLocal<Location>();
	
	public static void setStatusLoc(Location statusLoc) {
		instance.set(statusLoc);
	}

	public static Location getStatusLoc() {
		return instance.get();
	}

}