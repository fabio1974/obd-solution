package com.moveltrack.monitorsat.gate.threadlocal;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
public class Serial2 {

	public static final ThreadLocal<Byte> instance = new ThreadLocal<Byte>();
	
	public static void setSerial2(Byte serial1) {
		instance.set(serial1);
	}

	public static Byte getSerial2() {
		return instance.get();
	}

}