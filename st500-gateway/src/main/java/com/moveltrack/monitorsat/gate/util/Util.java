package com.moveltrack.monitorsat.gate.util;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.swing.text.MaskFormatter;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.moveltrack.monitorsat.gate.model.Location;
import com.moveltrack.monitorsat.gate.model.Location2;

/**
 * @author Barros
 *
 */
public class Util{
  
  public static <E> List<E> toList(Iterable<E> iterable) {
    if(iterable instanceof List) {
      return (List<E>) iterable;
    }
    ArrayList<E> list = new ArrayList<E>();
    if(iterable != null) {
      for(E e: iterable) {
        list.add(e);
      }
    }
    return list;
  }
  
	public static String formatarString(String texto, String mascara) {
		try {
			MaskFormatter mf = null;
			mf = new MaskFormatter(mascara);
			mf.setValueContainsLiteralCharacters(false);
			return mf.valueToString(texto);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static HttpSession session() {
	    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    return attr.getRequest().getSession(true); // true == allow create
	}
	
	public static Object getSessionAttribute(String attribute) {
		return session().getAttribute(attribute);
	}
	

	
	public static Date setTime( final Date date, final int hourOfDay, final int minute, final int second, final int ms )
	{
	    final GregorianCalendar gc = new GregorianCalendar();
	    gc.setTime( date );
	    gc.set( Calendar.HOUR_OF_DAY, hourOfDay );
	    gc.set( Calendar.MINUTE, minute );
	    gc.set( Calendar.SECOND, second );
	    gc.set( Calendar.MILLISECOND, ms );
	    return gc.getTime();
	}
	
	public static boolean isHistorico(Date t1){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_YEAR,-1);
		Date d = setTime(c.getTime(),0,0,0,0);
		return !t1.after(d);
	}
	
	public static Location location2ToLocation(Object obj) {
		if(obj==null)
			return null;
		Location loc = new Location();
		loc.setIgnition(((Location2)obj).getIgnition());
		loc.setLatitude(((Location2)obj).getLatitude());
		loc.setLongitude(((Location2)obj).getLongitude());
		loc.setVelocidade(((Location2)obj).getVelocidade());
		loc.setDateLocationInicio(((Location2)obj).getDateLocationInicio());
		loc.setDateLocation(((Location2)obj).getDateLocation());
		loc.setAlarm(((Location2)obj).getAlarm());
		loc.setAlarmType(((Location2)obj).getAlarmType());
		loc.setBattery(((Location2)obj).getBattery());
		loc.setBloqueio(((Location2)obj).getBloqueio());
		loc.setComando(((Location2)obj).getComando());
		loc.setGps(((Location2)obj).getGps());
		loc.setGsm(((Location2)obj).getGsm());
		loc.setId(((Location2)obj).getId());
		loc.setImei(((Location2)obj).getImei());
		loc.setMcc(((Location2)obj).getMcc());
		loc.setSatelites(((Location2)obj).getSatelites());
		loc.setSos(((Location2)obj).getSos());
		//loc.setVersion(((Location2)obj).getVersion());
		loc.setVolt(((Location2)obj).getVolt());
		return loc;
	}
	
	public static Location2 locationToLocation2(Location obj) {
		if(obj==null)
			return null;
		Location2 loc = new Location2();
		loc.setIgnition(obj.getIgnition());
		loc.setLatitude(obj.getLatitude());
		loc.setLongitude(obj.getLongitude());
		loc.setVelocidade(obj.getVelocidade());
		loc.setDateLocationInicio(obj.getDateLocationInicio());
		loc.setDateLocation(obj.getDateLocation());
		loc.setAlarm(obj.getAlarm());
		loc.setAlarmType(obj.getAlarmType());
		loc.setBattery(obj.getBattery());
		loc.setBloqueio(obj.getBloqueio());
		loc.setComando(obj.getComando());
		loc.setGps(obj.getGps());
		loc.setGsm(obj.getGsm());
		loc.setId((obj.getId()));
		loc.setImei(obj.getImei());
		loc.setMcc(obj.getMcc());
		loc.setSatelites(obj.getSatelites());
		loc.setSos(obj.getSos());
		//loc.setVersion(((Location2)obj).getVersion());
		loc.setVolt(obj.getVolt());
		return loc;
	}
	
}

   