package com.moveltrack.monitorsat.gate;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@SpringBootApplication
@EnableScheduling
public class St500Application {
	
	public static void main(String[] args) {
		SpringApplication.run(St500Application.class, args);
	}
		
	@Bean 
	public LocaleResolver localeResolver() {
		return new FixedLocaleResolver(new Locale("pt", "BR"));
	}

}
