package com.moveltrack.monitorsat.gate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.moveltrack.monitorsat.gate.model.Location2;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Repository
public interface Location2Rep extends JpaRepository<Location2,Long>, JpaSpecificationExecutor<Location2>  {

	


}
