package com.moveltrack.monitorsat.gate.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.moveltrack.monitorsat.gate.model.Rastreador;
import com.moveltrack.monitorsat.gate.model.Viatura;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */

@RepositoryRestResource(path = "viaturas")
public interface ViaturaRepository extends PagingAndSortingRepository<Viatura,Integer> , JpaSpecificationExecutor<Viatura>  {

	Viatura findByRastreador(Rastreador equipamento);


}