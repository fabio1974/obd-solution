package com.moveltrack.monitorsat.gate.util;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
public class Constantes {

	public static String SAVE = "Salvar";
	public static String CANCEL = "Sair";
	public static String DELETE = "Apagar";
	
	public final static int PORT_TK103 = 6214;
	public final static int PORT_GT06 =  6215;
	public final static int PORT_H02 =   6216;
	public final static int PORT_GT06Acurrate = 6217;
	public final static int PORT_XT009=  6218;
	public final static int PORT_TR02 =  6219;
	public final static int PORT_CRX1 =  6220;	
	public final static int PORT_JV200 =  6221;
	public final static int PORT_TK106 =  6222;
	public final static int PORT_CRXN =  6223;
	public final static int PORT_ST500 =  6224;
	
	
	public static final boolean DEBUG_CRX1 = false;
	public static final boolean DEBUG_GT06 = false;
	public static final boolean DEBUG_TK103 = false;
	public static final boolean DEBUG_TR02 = false;
	public static final boolean DEBUG_GT06Acurrate = false;
	public static final boolean DEBUG_XT009 = false;
	public static final boolean DEBUG_H02 = false;	
	public static final boolean DEBUG_JV200 = false;
	public static final boolean DEBUG_TK06 = false;
	public static final boolean DEBUG_CRXN = false;
	public static final boolean DEBUG_ST500 = true;

	
	
}
