package com.moveltrack.monitorsat.gate.model.st500;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Entity
public class ST500HTE {

	static String ST500HTEStr = "ST500HTE;205000050;07;657;20151023;09:43:45;12.28;4.1;1;1000;0;-22.881018;-047.070831" + 
			";-22.881018;-047.070831;000.000;000.000;0;0;000.000;000.000;000.000;000.000;000.000;000.000" + 
			";10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;10;7;4.2";

	public static void main(String[] args) {

		ST500HTE teste = parse(ST500HTEStr.split(";"));
		teste.getDriveHourMeter();

	}


	public static ST500HTE parse(String[] fields) {
		ST500HTE res = new ST500HTE();

		try {res.setSerial(fields[1]);}catch(Exception e){}
		try {res.setModel(fields[2]);}catch(Exception e){}
		try {res.setSwVersion(fields[3]);}catch(Exception e){}

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
			Date d1= format.parse(fields[4]+" "+fields[5]);
			Calendar c = Calendar.getInstance();
			c.setTime(d1);
			c.add(Calendar.MINUTE,-180);
			res.setDeviceDate(c.getTime());
		}catch(Exception e){}

		try {res.setVolt(Double.parseDouble(fields[6]));}catch(Exception e){}
		try {res.setBackupBatteryVoltage(Double.parseDouble(fields[7]));}catch(Exception e){}
		try {res.setOnline(Integer.parseInt(fields[8]));}catch(Exception e){}
		try {res.setDistance(Integer.parseInt(fields[9]));}catch(Exception e){}
		try {res.setTravelTime(Integer.parseInt(fields[10]));}catch(Exception e){}


		try {res.setLatitudeBegin(Double.parseDouble(fields[11]));}catch(Exception e){}
		try {res.setLongitudeBegin(Double.parseDouble(fields[12]));}catch(Exception e){}
		
		try {res.setLatitudeEnd(Double.parseDouble(fields[13]));}catch(Exception e){}
		try {res.setLongitudeEnd(Double.parseDouble(fields[14]));}catch(Exception e){}

		try {res.setAverageSpeed(Double.parseDouble(fields[15]));}catch(Exception e){}
		try {res.setMaxSpeed(Double.parseDouble(fields[16]));}catch(Exception e){}
		try {res.setTimeOverSpeed(Integer.parseInt(fields[17]));}catch(Exception e){}
		try {res.setParkinTime(Integer.parseInt(fields[18]));}catch(Exception e){}
		
		try {res.setMaxFastAcellerationG(Double.parseDouble(fields[19]));}catch(Exception e){}
		try {res.setAverageFastAcellerationG(Double.parseDouble(fields[20]));}catch(Exception e){}
		
		try {res.setMaxHarshBrakeG(Double.parseDouble(fields[21]));}catch(Exception e){}
		try {res.setAverageHarshBrakeG(Double.parseDouble(fields[22]));}catch(Exception e){}

		try {res.setMaxSharpTurnG(Double.parseDouble(fields[23]));}catch(Exception e){}
		try {res.setAverageSharpTurnG(Double.parseDouble(fields[24]));}catch(Exception e){}
		
		try {res.setHistogramSpeed000_009(Integer.parseInt(fields[25]));}catch(Exception e){}
		try {res.setHistogramSpeed010_019(Integer.parseInt(fields[26]));}catch(Exception e){}
		try {res.setHistogramSpeed020_029(Integer.parseInt(fields[27]));}catch(Exception e){}
		try {res.setHistogramSpeed030_039(Integer.parseInt(fields[28]));}catch(Exception e){}
		try {res.setHistogramSpeed040_049(Integer.parseInt(fields[29]));}catch(Exception e){}
		try {res.setHistogramSpeed050_059(Integer.parseInt(fields[30]));}catch(Exception e){}
		try {res.setHistogramSpeed060_069(Integer.parseInt(fields[31]));}catch(Exception e){}
		try {res.setHistogramSpeed070_079(Integer.parseInt(fields[32]));}catch(Exception e){}
		try {res.setHistogramSpeed080_089(Integer.parseInt(fields[33]));}catch(Exception e){}
		try {res.setHistogramSpeed090_099(Integer.parseInt(fields[34]));}catch(Exception e){}
		try {res.setHistogramSpeed100_109(Integer.parseInt(fields[35]));}catch(Exception e){}
		try {res.setHistogramSpeed110_119(Integer.parseInt(fields[36]));}catch(Exception e){}
		try {res.setHistogramSpeed120_129(Integer.parseInt(fields[37]));}catch(Exception e){}
		try {res.setHistogramSpeed130_139(Integer.parseInt(fields[38]));}catch(Exception e){}
		try {res.setHistogramSpeed140_149(Integer.parseInt(fields[39]));}catch(Exception e){}
		try {res.setHistogramSpeed150_159(Integer.parseInt(fields[40]));}catch(Exception e){}
		try {res.setHistogramSpeed160_169(Integer.parseInt(fields[41]));}catch(Exception e){}
		try {res.setHistogramSpeed170_179(Integer.parseInt(fields[42]));}catch(Exception e){}
		try {res.setHistogramSpeed180_189(Integer.parseInt(fields[43]));}catch(Exception e){}
		try {res.setHistogramSpeed190_199(Integer.parseInt(fields[44]));}catch(Exception e){}
		try {res.setHistogramSpeedOver200(Integer.parseInt(fields[45]));}catch(Exception e){}

		try {res.setDriveHourMeter(Integer.parseInt(fields[46]));}catch(Exception e){}

		return res;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition="serial")
	private Long id;

	private String serial;
	private String model;
	private String swVersion;
	
	private Date deviceDate;

	private Double volt;
	private Double backupBatteryVoltage;
	private Integer online;
	private Integer distance;
	private Integer travelTime;

	private Double latitudeBegin;
	private Double longitudeBegin;
	private Double latitudeEnd;
	private Double longitudeEnd;
	
	

	private Double averageSpeed;
	private Double maxSpeed;
	private Integer timeOverSpeed;
	private Integer parkinTime;

	private Double maxFastAcellerationG;
	private Double averageFastAcellerationG;

	private Double maxHarshBrakeG;
	private Double averageHarshBrakeG;

	private Double maxSharpTurnG;
	private Double averageSharpTurnG;

	private Integer histogramSpeed000_009;
	private Integer histogramSpeed010_019;
	private Integer histogramSpeed020_029;
	private Integer histogramSpeed030_039;
	private Integer histogramSpeed040_049;
	private Integer histogramSpeed050_059;
	private Integer histogramSpeed060_069;
	private Integer histogramSpeed070_079;
	private Integer histogramSpeed080_089;
	private Integer histogramSpeed090_099;
	private Integer histogramSpeed100_109;
	private Integer histogramSpeed110_119;
	private Integer histogramSpeed120_129;
	private Integer histogramSpeed130_139;
	private Integer histogramSpeed140_149;
	private Integer histogramSpeed150_159;
	private Integer histogramSpeed160_169;
	private Integer histogramSpeed170_179;
	private Integer histogramSpeed180_189;
	private Integer histogramSpeed190_199;
	private Integer histogramSpeedOver200;
	private Integer driveHourMeter;

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getSerial() {
		return serial;
	}


	public void setSerial(String serial) {
		this.serial = serial;
	}


	public String getModel() {
		return model;
	}
	
	public Date getDeviceDate() {
		return deviceDate;
	}


	public void setDeviceDate(Date deviceDate) {
		this.deviceDate = deviceDate;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getSwVersion() {
		return swVersion;
	}


	public void setSwVersion(String swVersion) {
		this.swVersion = swVersion;
	}


	public Double getVolt() {
		return volt;
	}


	public void setVolt(Double volt) {
		this.volt = volt;
	}


	public Double getBackupBatteryVoltage() {
		return backupBatteryVoltage;
	}


	public void setBackupBatteryVoltage(Double backupBatteryVoltage) {
		this.backupBatteryVoltage = backupBatteryVoltage;
	}


	public Integer getOnline() {
		return online;
	}


	public void setOnline(Integer online) {
		this.online = online;
	}


	public Integer getDistance() {
		return distance;
	}


	public void setDistance(Integer distance) {
		this.distance = distance;
	}


	public Integer getTravelTime() {
		return travelTime;
	}


	public void setTravelTime(Integer travelTime) {
		this.travelTime = travelTime;
	}


	public Double getLatitudeBegin() {
		return latitudeBegin;
	}


	public void setLatitudeBegin(Double latitudeBegin) {
		this.latitudeBegin = latitudeBegin;
	}


	public Double getLongitudeBegin() {
		return longitudeBegin;
	}


	public void setLongitudeBegin(Double longitudeBegin) {
		this.longitudeBegin = longitudeBegin;
	}


	public Double getLatitudeEnd() {
		return latitudeEnd;
	}


	public void setLatitudeEnd(Double latitudeEnd) {
		this.latitudeEnd = latitudeEnd;
	}


	public Double getLongitudeEnd() {
		return longitudeEnd;
	}


	public void setLongitudeEnd(Double longitudeEnd) {
		this.longitudeEnd = longitudeEnd;
	}


	public Double getAverageSpeed() {
		return averageSpeed;
	}


	public void setAverageSpeed(Double averageSpeed) {
		this.averageSpeed = averageSpeed;
	}


	public Double getMaxSpeed() {
		return maxSpeed;
	}


	public void setMaxSpeed(Double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}


	public Integer getTimeOverSpeed() {
		return timeOverSpeed;
	}


	public void setTimeOverSpeed(Integer timeOverSpeed) {
		this.timeOverSpeed = timeOverSpeed;
	}


	public Integer getParkinTime() {
		return parkinTime;
	}


	public void setParkinTime(Integer parkinTime) {
		this.parkinTime = parkinTime;
	}



	public Double getMaxFastAcellerationG() {
		return maxFastAcellerationG;
	}


	public void setMaxFastAcellerationG(Double maxFastAcellerationG) {
		this.maxFastAcellerationG = maxFastAcellerationG;
	}


	public Double getAverageFastAcellerationG() {
		return averageFastAcellerationG;
	}


	public void setAverageFastAcellerationG(Double averageFastAcellerationG) {
		this.averageFastAcellerationG = averageFastAcellerationG;
	}


	public Double getMaxHarshBrakeG() {
		return maxHarshBrakeG;
	}


	public void setMaxHarshBrakeG(Double maxHarshBrakeG) {
		this.maxHarshBrakeG = maxHarshBrakeG;
	}


	public Double getAverageHarshBrakeG() {
		return averageHarshBrakeG;
	}


	public void setAverageHarshBrakeG(Double averageHarshBrakeG) {
		this.averageHarshBrakeG = averageHarshBrakeG;
	}


	public Double getMaxSharpTurnG() {
		return maxSharpTurnG;
	}


	public void setMaxSharpTurnG(Double maxSharpTurnG) {
		this.maxSharpTurnG = maxSharpTurnG;
	}


	public Double getAverageSharpTurnG() {
		return averageSharpTurnG;
	}


	public void setAverageSharpTurnG(Double averageSharpTurnG) {
		this.averageSharpTurnG = averageSharpTurnG;
	}


	public Integer getHistogramSpeed000_009() {
		return histogramSpeed000_009;
	}


	public void setHistogramSpeed000_009(Integer histogramSpeed000_009) {
		this.histogramSpeed000_009 = histogramSpeed000_009;
	}


	public Integer getHistogramSpeed010_019() {
		return histogramSpeed010_019;
	}


	public void setHistogramSpeed010_019(Integer histogramSpeed010_019) {
		this.histogramSpeed010_019 = histogramSpeed010_019;
	}


	public Integer getHistogramSpeed020_029() {
		return histogramSpeed020_029;
	}


	public void setHistogramSpeed020_029(Integer histogramSpeed020_029) {
		this.histogramSpeed020_029 = histogramSpeed020_029;
	}


	public Integer getHistogramSpeed030_039() {
		return histogramSpeed030_039;
	}


	public void setHistogramSpeed030_039(Integer histogramSpeed030_039) {
		this.histogramSpeed030_039 = histogramSpeed030_039;
	}


	public Integer getHistogramSpeed040_049() {
		return histogramSpeed040_049;
	}


	public void setHistogramSpeed040_049(Integer histogramSpeed040_049) {
		this.histogramSpeed040_049 = histogramSpeed040_049;
	}


	public Integer getHistogramSpeed050_059() {
		return histogramSpeed050_059;
	}


	public void setHistogramSpeed050_059(Integer histogramSpeed050_059) {
		this.histogramSpeed050_059 = histogramSpeed050_059;
	}


	public Integer getHistogramSpeed060_069() {
		return histogramSpeed060_069;
	}


	public void setHistogramSpeed060_069(Integer histogramSpeed060_069) {
		this.histogramSpeed060_069 = histogramSpeed060_069;
	}


	public Integer getHistogramSpeed070_079() {
		return histogramSpeed070_079;
	}


	public void setHistogramSpeed070_079(Integer histogramSpeed070_079) {
		this.histogramSpeed070_079 = histogramSpeed070_079;
	}


	public Integer getHistogramSpeed080_089() {
		return histogramSpeed080_089;
	}


	public void setHistogramSpeed080_089(Integer histogramSpeed080_089) {
		this.histogramSpeed080_089 = histogramSpeed080_089;
	}


	public Integer getHistogramSpeed090_099() {
		return histogramSpeed090_099;
	}


	public void setHistogramSpeed090_099(Integer histogramSpeed090_099) {
		this.histogramSpeed090_099 = histogramSpeed090_099;
	}


	public Integer getHistogramSpeed100_109() {
		return histogramSpeed100_109;
	}


	public void setHistogramSpeed100_109(Integer histogramSpeed100_109) {
		this.histogramSpeed100_109 = histogramSpeed100_109;
	}


	public Integer getHistogramSpeed110_119() {
		return histogramSpeed110_119;
	}


	public void setHistogramSpeed110_119(Integer histogramSpeed110_119) {
		this.histogramSpeed110_119 = histogramSpeed110_119;
	}


	public Integer getHistogramSpeed120_129() {
		return histogramSpeed120_129;
	}


	public void setHistogramSpeed120_129(Integer histogramSpeed120_129) {
		this.histogramSpeed120_129 = histogramSpeed120_129;
	}


	public Integer getHistogramSpeed130_139() {
		return histogramSpeed130_139;
	}


	public void setHistogramSpeed130_139(Integer histogramSpeed130_139) {
		this.histogramSpeed130_139 = histogramSpeed130_139;
	}


	public Integer getHistogramSpeed140_149() {
		return histogramSpeed140_149;
	}


	public void setHistogramSpeed140_149(Integer histogramSpeed140_149) {
		this.histogramSpeed140_149 = histogramSpeed140_149;
	}


	public Integer getHistogramSpeed150_159() {
		return histogramSpeed150_159;
	}


	public void setHistogramSpeed150_159(Integer histogramSpeed150_159) {
		this.histogramSpeed150_159 = histogramSpeed150_159;
	}


	public Integer getHistogramSpeed160_169() {
		return histogramSpeed160_169;
	}


	public void setHistogramSpeed160_169(Integer histogramSpeed160_169) {
		this.histogramSpeed160_169 = histogramSpeed160_169;
	}


	public Integer getHistogramSpeed170_179() {
		return histogramSpeed170_179;
	}


	public void setHistogramSpeed170_179(Integer histogramSpeed170_179) {
		this.histogramSpeed170_179 = histogramSpeed170_179;
	}


	public Integer getHistogramSpeed180_189() {
		return histogramSpeed180_189;
	}


	public void setHistogramSpeed180_189(Integer histogramSpeed180_189) {
		this.histogramSpeed180_189 = histogramSpeed180_189;
	}


	public Integer getHistogramSpeed190_199() {
		return histogramSpeed190_199;
	}


	public void setHistogramSpeed190_199(Integer histogramSpeed190_199) {
		this.histogramSpeed190_199 = histogramSpeed190_199;
	}


	public Integer getHistogramSpeedOver200() {
		return histogramSpeedOver200;
	}


	public void setHistogramSpeedOver200(Integer histogramSpeedOver200) {
		this.histogramSpeedOver200 = histogramSpeedOver200;
	}


	public Integer getDriveHourMeter() {
		return driveHourMeter;
	}


	public void setDriveHourMeter(Integer driveHourMeter) {
		this.driveHourMeter = driveHourMeter;
	}





}
