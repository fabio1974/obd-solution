package com.moveltrack.monitorsat.gate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.moveltrack.monitorsat.gate.util.Constantes;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
@Component
public class StartApplicationRunner implements ApplicationRunner {

	@Autowired ReadingProtocolListener t10;	
	@Autowired ReadingProtocolHandlerST500  rphST500;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
    	t10.setPh(rphST500); 
    	t10.setPort(Constantes.PORT_ST500);	
    	t10.start();
    }
}