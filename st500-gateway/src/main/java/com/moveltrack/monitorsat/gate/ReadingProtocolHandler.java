package com.moveltrack.monitorsat.gate;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.BitSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moveltrack.monitorsat.gate.model.Location;
import com.moveltrack.monitorsat.gate.model.RastreadorTipo;
import com.moveltrack.monitorsat.gate.repository.Location2Rep;
import com.moveltrack.monitorsat.gate.repository.LocationRep;
import com.moveltrack.monitorsat.gate.util.Util;
/**
 * 
 * @author mailto:jlimasa@gmail.com
 *
 */


@Service
public abstract class ReadingProtocolHandler implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	boolean DEBUG_SERVICE = false;
	
	@Autowired LocationRep locationRep;
	@Autowired Location2Rep location2Rep;

	public ReadingProtocolHandler() {
	}

	public abstract void startReading(Socket clientSocket) throws IOException ;
	
	public void sendToTerminal(byte[] response,Socket socket) {
		try {
			if(socket!=null && socket.isConnected() && !socket.isClosed()) {
				OutputStream out = socket.getOutputStream();
				out.write(response);
			}
		} catch (IOException e) {
			if(DEBUG_SERVICE)
				e.printStackTrace();
		}
	}
	
	public void saveLocation(Location loc, RastreadorTipo modelo) {
		saveLocation(loc);
	}
	
	Location lastLocation;
	
	private void saveLocation(Location loc){
		if(lastLocation==null || 
				lastLocation.getLatitude()!=loc.getLatitude() || 
				lastLocation.getLongitude()!=loc.getLongitude() || 
				lastLocation.getDateLocation().getTime()!=loc.getDateLocation().getTime()){

			if(loc.getLatitude()!=0){
				locationRep.save(loc);
				location2Rep.save(Util.locationToLocation2(loc));
				lastLocation = loc;
			}
		}
	}
	
	public void printBuff(Boolean DEBUG_MODE,byte...buff){
		for (int i = 0; i < buff.length && DEBUG_MODE; i++) {
			System.out.printf("%02x ",buff[i]);
		}
		if(DEBUG_MODE)
			System.out.println();
	}	

	
    public BitSet fromByte(byte b)  
    {  
        BitSet bits = new BitSet(8);  
        for (int i = 0; i < 8; i++)  
        {  
            bits.set(i, (b & 1) == 1);  
            b >>= 1;  
        }  
        return bits;  
    }	
    
	public void printSocketStatus(Boolean DEBUG_MODE,String title,Socket socket) {
		if(DEBUG_MODE) {
			System.out.println(title+ " "+(socket!=null?"not null":"null"));
			if(socket!=null) {
				System.out.println(title+ " "+(socket.isConnected()?"connected":"not conected"));
				System.out.println(title+ " "+(socket.isClosed()?"closed":"not closed"));
				System.out.println(title+ " "+"Inet Address: "+socket.getInetAddress());
			}
		}
	}    
}