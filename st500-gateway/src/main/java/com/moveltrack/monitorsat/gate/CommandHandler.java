package com.moveltrack.monitorsat.gate;

import java.net.Socket;
import java.util.BitSet;
import org.springframework.stereotype.Service;

/**
 * 
 * @author mailto:jlimasa@gmail.com
 *
 */



@Service
public abstract class CommandHandler {
	
	public CommandHandler() {
	}

	private boolean isSocketOk(Socket socket) {
		return !socket.isOutputShutdown()&&socket.isBound()&&!socket.isClosed()&&socket.isConnected();
	}
	
	public void printBuff(Boolean DEBUG_MODE,byte...buff){
		for (int i = 0; i < buff.length && DEBUG_MODE; i++) {
			System.out.printf("%02x ",buff[i]);
		}
		if(DEBUG_MODE)
			System.out.println();
	}	

	public BitSet fromByte(byte b)  
	{  
		BitSet bits = new BitSet(8);  
		for (int i = 0; i < 8; i++)  
		{  
			bits.set(i, (b & 1) == 1);  
			b >>= 1;  
		}  
		return bits;  
	}	

	public void printSocketStatus(Boolean DEBUG_MODE,String title,Socket socket) {
		if(DEBUG_MODE) {
			System.out.println(title+ " "+(socket!=null?"not null":"null"));
			if(socket!=null) {
				System.out.println(title+ " "+(socket.isInputShutdown()?"input shutdown":"input ok"));
				System.out.println(title+ " "+(socket.isOutputShutdown()?"output shutdown":"output ok"));
				System.out.println(title+ " "+(socket.isBound()?"bound":"not bound"));
				System.out.println(title+ " "+(socket.isConnected()?"connected":"not conected"));
				System.out.println(title+ " "+(socket.isClosed()?"closed":"not closed"));
				System.out.println(title+ " "+"Inet Address: "+socket.getInetAddress());
			}
		}
	}    
}