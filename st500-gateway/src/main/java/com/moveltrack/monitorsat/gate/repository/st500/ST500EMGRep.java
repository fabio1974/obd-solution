package com.moveltrack.monitorsat.gate.repository.st500;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.moveltrack.monitorsat.gate.model.st500.ST500EMG;

@Repository
public interface ST500EMGRep extends JpaRepository<ST500EMG,Long>{

}
