package com.moveltrack.monitorsat.gate.repository;

import java.util.Date;
import java.util.List;

import com.moveltrack.monitorsat.gate.model.Location;
import com.moveltrack.monitorsat.gate.model.Viatura;
/**
 * 
 * @author jlimasa@gmail.com
 *
 */
public interface LocationRepCustom {
	public List<Object> findLocationsByVeiculoInicioFim(Viatura viatura,Date inicio, Date fim);
	public Location getLastLocationFromVeiculo(Viatura viatura) ;
}
