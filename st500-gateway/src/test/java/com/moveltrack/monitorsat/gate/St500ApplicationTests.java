package com.moveltrack.monitorsat.gate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 
 * @author mailto:jlimasa@gmail.com
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class St500ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
